DROP TABLE IF EXISTS Client ; 
CREATE TABLE Client (id_Client SERIAL(100) NOT NULL, 
id_Commentaires **NOT FOUND**(100), 
impression_id_impression **NOT FOUND**(100), 
PRIMARY KEY (id_Client)); 
DROP TABLE IF EXISTS Commentaires ; 
CREATE TABLE Commentaires (id_Commentaires SERIAL(50) NOT NULL, 
texte_Commentaires TEXT, PRIMARY KEY (id_Commentaires)); 
DROP TABLE IF EXISTS Impression ; 
CREATE TABLE Impression (id_Impression SERIAL(100) NOT NULL, 
type_Impression VARCHAR, 
nbcouleur_Impression INT(6), 
nbimpression_Impression INT(100), 
client_id_client **NOT FOUND**(100), 
bord_rond_Options **NOT FOUND**(100), 
support_id_support **NOT FOUND**(100), 
PRIMARY KEY (id_Impression)); 
DROP TABLE IF EXISTS Options ; 
CREATE TABLE Options (bord_rond_Options BOOLEAN NOT NULL, 
pointilles_Options BOOLEAN, 
plieuse_Options BOOLEAN, 
PRIMARY KEY (bord_rond_Options)); 
DROP TABLE IF EXISTS Support ; 
CREATE TABLE Support (id_Support SERIAL(100) NOT NULL, 
couleur_Impression VARCHAR(15), 
type_Support VARCHAR(20), 
tailleLongueur INT(42), 
tailleLargeur_Support INT(60), 
impression_id_impression **NOT FOUND**(100), 
PRIMARY KEY (id_Support)); 
DROP TABLE IF EXISTS Coordonnees ; 
CREATE TABLE Coordonnees (id_Coordonnees SERIAL(100) NOT NULL, 
numRue_Coordonnees INT(1000), 
nomRue_Coordonnees VARCHAR(100), 
codePostal_Coordonnees VARCHAR(5), 
ville_Coordonnees VARCHAR(40), 
numTel_Coordonnees VARCHAR(15), 
PRIMARY KEY (id_Coordonnees)); 
DROP TABLE IF EXISTS Fichiers ; 
CREATE TABLE Fichiers (id_Fichiers SERIAL(100) NOT NULL, 
projet_Fichiers INT(1000), 
PRIMARY KEY (id_Fichiers)); 
DROP TABLE IF EXISTS Administrateur ; 
CREATE TABLE Administrateur (id_Administrateur SERIAL(5) NOT NULL, 
galerieimage_id_galerieimage **NOT FOUND**(5), 
PRIMARY KEY (id_Administrateur)); 
DROP TABLE IF EXISTS News ; 
CREATE TABLE News (id_News SERIAL(1000) NOT NULL, texte_News TEXT, PRIMARY KEY (id_News)); 
DROP TABLE IF EXISTS GalerieImage ; 
CREATE TABLE GalerieImage (id_GalerieImage SERIAL(5) NOT NULL, 
photo_id_GalerieImage INT, administrateur_id_administrateur **NOT FOUND**(5), 
PRIMARY KEY (id_GalerieImage)); 
DROP TABLE IF EXISTS Connexion ; 
CREATE TABLE Connexion (login_Connexion VARCHAR(15) NOT NULL, 
password_Connexion VARCHAR(15), 
PRIMARY KEY (login_Connexion)); 
DROP TABLE IF EXISTS possede ; 
CREATE TABLE possede (id_Client **NOT FOUND**(100) NOT NULL, 
id_Coordonnees **NOT FOUND**(100) NOT NULL, 
id_Fichiers **NOT FOUND**(100) NOT NULL, 
PRIMARY KEY (id_Client,  id_Coordonnees,  id_Fichiers)); 
DROP TABLE IF EXISTS edite ; 
CREATE TABLE edite (id_Administrateur **NOT FOUND**(5) NOT NULL, 
id_Impression **NOT FOUND**(5) NOT NULL, 
PRIMARY KEY (id_Administrateur,  id_Impression)); 
DROP TABLE IF EXISTS publie ; 
CREATE TABLE publie (id_Administrateur **NOT FOUND**(5) NOT NULL, 
id_News **NOT FOUND**(5) NOT NULL, 
PRIMARY KEY (id_Administrateur,  id_News)); 
DROP TABLE IF EXISTS valide ; 
CREATE TABLE valide (id_Administrateur **NOT FOUND**(5) NOT NULL, id_Client **NOT FOUND**(5) NOT NULL, 
PRIMARY KEY (id_Administrateur,  id_Client)); 
DROP TABLE IF EXISTS seConnecte ; 
CREATE TABLE seConnecte (id_Client **NOT FOUND**(100) NOT NULL, 
id_Administrateur **NOT FOUND**(100) NOT NULL, 
login_Connexion **NOT FOUND**(100) NOT NULL, 
PRIMARY KEY (id_Client,  id_Administrateur,  login_Connexion)); 
ALTER TABLE Client ADD CONSTRAINT FK_Client_id_Commentaires FOREIGN KEY (id_Commentaires) REFERENCES Commentaires (id_Commentaires); 
ALTER TABLE Client ADD CONSTRAINT FK_Client_impression_id_impression FOREIGN KEY (impression_id_impression) REFERENCES Impression (id_Impression); 
ALTER TABLE Impression ADD CONSTRAINT FK_Impression_client_id_client FOREIGN KEY (client_id_client) REFERENCES Client (id_Client); 
ALTER TABLE Impression ADD CONSTRAINT FK_Impression_bord_rond_Options FOREIGN KEY (bord_rond_Options) REFERENCES Options (bord_rond_Options); 
ALTER TABLE Impression ADD CONSTRAINT FK_Impression_support_id_support FOREIGN KEY (support_id_support) REFERENCES Support (id_Support); 
ALTER TABLE Support ADD CONSTRAINT FK_Support_impression_id_impression FOREIGN KEY (impression_id_impression) REFERENCES Impression (id_Impression); 
ALTER TABLE Administrateur ADD CONSTRAINT FK_Administrateur_galerieimage_id_galerieimage FOREIGN KEY (galerieimage_id_galerieimage) REFERENCES GalerieImage (id_GalerieImage); 
ALTER TABLE GalerieImage ADD CONSTRAINT FK_GalerieImage_administrateur_id_administrateur FOREIGN KEY (administrateur_id_administrateur) REFERENCES Administrateur (id_Administrateur); 
ALTER TABLE possede ADD CONSTRAINT FK_possede_id_Client FOREIGN KEY (id_Client) REFERENCES Client (id_Client); 
ALTER TABLE possede ADD CONSTRAINT FK_possede_id_Coordonnees FOREIGN KEY (id_Coordonnees) REFERENCES Coordonnees (id_Coordonnees); 
ALTER TABLE possede ADD CONSTRAINT FK_possede_id_Fichiers FOREIGN KEY (id_Fichiers) REFERENCES Fichiers (id_Fichiers); 
ALTER TABLE edite ADD CONSTRAINT FK_edite_id_Administrateur FOREIGN KEY (id_Administrateur) REFERENCES Administrateur (id_Administrateur); 
ALTER TABLE edite ADD CONSTRAINT FK_edite_id_Impression FOREIGN KEY (id_Impression) REFERENCES Impression (id_Impression); 
ALTER TABLE publie ADD CONSTRAINT FK_publie_id_Administrateur FOREIGN KEY (id_Administrateur) REFERENCES Administrateur (id_Administrateur); 
ALTER TABLE publie ADD CONSTRAINT FK_publie_id_News FOREIGN KEY (id_News) REFERENCES News (id_News); 
ALTER TABLE valide ADD CONSTRAINT FK_valide_id_Administrateur FOREIGN KEY (id_Administrateur) REFERENCES Administrateur (id_Administrateur); 
ALTER TABLE valide ADD CONSTRAINT FK_valide_id_Client FOREIGN KEY (id_Client) REFERENCES Client (id_Client); 
ALTER TABLE seConnecte ADD CONSTRAINT FK_seConnecte_id_Client FOREIGN KEY (id_Client) REFERENCES Client (id_Client); 
ALTER TABLE seConnecte ADD CONSTRAINT FK_seConnecte_id_Administrateur FOREIGN KEY (id_Administrateur) REFERENCES Administrateur (id_Administrateur); 
ALTER TABLE seConnecte ADD CONSTRAINT FK_seConnecte_login_Connexion FOREIGN KEY (login_Connexion) REFERENCES Connexion (login_Connexion); 