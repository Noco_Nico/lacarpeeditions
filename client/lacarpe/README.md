Projet : La Carpe Editions
Auteurs : Nicolas Dziamski
Descripition : site web présentant les activités de l'atelier de sérigraphie la Carpe.
La section Admin permet de gérer les pages actualites, page perso et recevoir des commandes des utilisateurs.
La section utilisateur permet de passer des commandes ainsi que d'envoyer des fichiers.

Technologies et outils utilis�es : React /nodejs /postgresql

Environement necessaire : npm

Installation :


Se rendre à l'aide de l'invite de commandes / terminal dans le dossier client

- executer npm install
- executer npm start


Fonctionnalités : 


- [ ] Log administrateur
- [ ] Log utilisateur
- [ ] Visualiser tout les utilisateurs 
- [ ] Ajouter un utilisateur
- [ ] Recevoir une commande
- [ ] Envoyer une commande
- [ ] Joindre un commentaire à sa commande
- [ ] Envoyer un fichier 
- [ ] Se déconnecter 
- [ ] Ajouter un billet sur la page actualités
- [ ] Supprimer un billet sur la page actualités
- [ ] Ajouter une image à sa gallerie
- [ ] Supprimer une image de sa gallerie
- [ ] Déplacer une image de sa gallerie
- [ ] Modifier une salle
- [ ] Reserver une salle


