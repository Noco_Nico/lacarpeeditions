import React, { Component, Fragment } from "react";
import Slider from "react-slick";
import carte from "../images/atelierCarousel/resized000.jpg";
import ardec from "../images/atelierCarousel/resized003.jpg";
import dajok from "../images/atelierCarousel/resized002.jpg";
import emulsion from "../images/atelierCarousel/resized008.jpg";
import amo from "../images/atelierCarousel/resized004.jpg";
import lyon from "../images/atelierCarousel/resized005.jpg";
import geraldine from "../images/atelierCarousel/resized009.jpg";
import apach from "../images/atelierCarousel/resized007.jpg";

export default class Carousel extends Component {
  render() {
    const settings = {
      dots: true,
      infinite: true,
      speed: 2000,
      slidesToShow: 3,
      slidesToScroll: 3,
      autoplay: true,
      autoplaySpeed: 4000,
      cssEase: "linear"
    };
    return (
      <Fragment>
        <Slider {...settings}>
          <div>
            <img src={carte}
              height={400}
              width={600} alt="carte" />
          </div>
          <div>
            <img src={emulsion}
              height={400}
              width={600} alt="emulsion" />
          </div>
          <div>
            <img src={apach}
              height={400}
              width={400} alt="apach" />
          </div>
          <div>
            <img src={ardec}
              height={400}
              width={600} alt="ardec" />
          </div>
          <div>
            <img src={amo}
              height={400}
              width={600} alt="amo" />
          </div>
          <div>
            <img src={lyon}
              height={400}
              width={600} alt="lyon" />
          </div>
          <div>
            <img src={geraldine}
              height={400}
              width={600} alt="geraldine" />
          </div>
          <div>
            <img src={dajok}
              height={400}
              width={600} alt="dajok" />
          </div>
        </Slider>
      </Fragment>
    );
  }
}