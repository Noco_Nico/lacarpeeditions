import React, { Fragment } from "react";
import papier from "../images/papier.jpg";

export default function Description() {
    return (
        <Fragment>
            <div className="ml-auto" >
                <p className="pt-5" />
                <div className="container">
                    <div className="row">
                        <p id="txtContenu" className="col-md-6">
                            <h3 className="text-center" >DES TIRAGES D'ART SUR MESURE</h3>
                            <p className="pt-2" />

Spécialisés depuis 2007 dans la sérigraphie artistique, nous avons acquis et développé un savoir-faire et un équipement nous permettant de proposer des tirages d’art d’une grande qualité, fidèles aux attentes des artistes et collectionneurs.
La technique de la sérigraphie, remarquable de par son efficacité, sa flexibilité, son rendu et sa tenue dans le temps est reconnue depuis de nombreuses années par les artistes du monde entier pour la production d’œuvres originales et de reproductions.
Elle est à ce titre reconnue officiellement comme technique artisanale de production d’estampes originales.

</p>
                        <p className="pt-4" />
                        <p id="txtContenu" className="col-md-6">
                            <h3 className="text-center">IMPRIMER SUR TOUT CE QUI EST PLAT</h3>
                            <p className="pt-2" />

Travaillant avec de nombreux fournisseurs de papiers, nous disposons d’un large éventail de papiers pour la production d’œuvres imprimées (papiers d’art, papiers japonais, papiers techniques…).
Nous imprimons également sur de nombreux autres supports : bois, verre, métaux, textiles, cuirs, matières synthétiques.
Sans cesse à la recherche de réponses spécifiques aux défis lancés par la production artistique et en complément d’une large gamme d’encres techniques (encres à gratter, encres thermochromiques, phosphorescentes, encapsulées…), nous sommes régulièrement amenés à travailler avec des solutions imaginées sur mesure et préparées avec les artistes à l’atelier (impression à base de matières organiques, flocage de poudres et de pigments, dorure à la feuille…).

</p>
                    </div>
                </div>
            </div>
           
            <div className="ml-auto" >
                <p className="pt-5" />
                <div className="container">
                    <div className="row">
                        <p id="txtContenu" className="col-md-12">
                            <h3 className="text-center">LA SÉRIGRAPHIE, QU'EST-CE QUE C'EST ?</h3>
                            <p className="pt-2" />
La sérigraphie est une technique d’imprimerie qui utilise des pochoirs (à l'origine, des écrans de soie) interposés entre l’encre et le support. Les supports utilisés peuvent être variés (papier, carton, textile, métal, verre, bois, etc.).
La sérigraphie fut créée par les Chinois durant la dynastie Song (960-1279) et se répandit dans les pays voisins.
La forte émigration chinoise vers les États-Unis au XIXe siècle marqua l’entrée de la sérigraphie dans l’ère moderne et favorisa son éclosion outre-Atlantique. L’engouement fut immédiat et la technique se modernisa, sous l’impulsion d’une industrie américaine très performante. Le racloir supplanta le rouleau pour l’application de l’encre et le Nylon fit oublier la soie en guise d’écran. Andy Warhol et Roy Lichtenstein s’adonnèrent sans modération à cette technique et lui donnèrent ses lettres de noblesse.
Lors de la Seconde Guerre mondiale, les soldats américains diffusèrent ce procédé très en vogue sur le continent européen. Chaque campement américain comportait un atelier de sérigraphie pour le marquage des véhicules militaires et la signalétique des camps. De nombreux artistes, dont Henri Matisse, furent séduits par ce nouveau mode d’expression. Les affiches sérigraphiques réalisées par les étudiants contestataires de Mai 68 en France élevèrent cet art au rang de mythe.
<p className="pt-5" /></p>

<div className="container-fluid text-center">
<img src ={papier}
height={600}
width={740} alt="fond"
/>
</div>
<p id="txtContenu" className="col-md-12">
    <p className="pt-5" />
À la fin des années 1970, la sérigraphie demeure très présente dans de nombreuses productions et les sérigraphies sont partout autour de nous : les panneaux signalétiques, les autocollants, les CD, les affiches de concert, les vêtements et les matériels industriels, entre autres, arborent des réalisations et motifs sérigraphiques. Cette technique peut être mise en œuvre sur papier, textile, circuit imprimé, verre, céramique, bois et métal et tout autres supports ou volumes à plat (formes géométriques régulières).
Elle autorise un fort dépôt d'encre qui garantit non seulement une couleur intense qui dure dans le temps mais également une bonne opacité.
Elle est intéressante économiquement même pour de courts tirages (mais l'impression numérique hausse le seuil de rentabilité).
La sérigraphie est utilisée pour les objets en volume, des supports non flexibles, ou des supports flexibles tendus, tels qu'un tissu.
Il est également possible de faire de la sérigraphie sur des aliments (pâtisserie) : dans ce cas, on utilise du sucre ou des colorants comestibles.
</p>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}