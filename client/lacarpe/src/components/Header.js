import React, { Fragment } from "react";
import logo from '../images/logo.png';
import { Link } from "react-router-dom";


export default function Header() {
  return (
    <Fragment>
      <nav className="navbar navbar-expand-sm ">
        <a className="navbar-brand " href="/">
          <img src={logo} alt="LaCarpe" />
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse " id="navbarSupportedContent">
          <ul className="navbar-nav ml-auto text-uppercase">
            <li className="nav-item active">
              <a className="nav-link" href="/actualites">
                <Link to="/"><h6>Actualites</h6></Link> <span className="sr-only">(current)</span>
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="/artistes">
                <Link to="/artistes"><h6>Artistes</h6></Link>
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="/atelier">
                <Link to="/atelier"><h6>Atelier</h6></Link>
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="https://amoze.bigcartel.com/" target="_blank">
                <h6>Boutique</h6>
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="/connexion">
                <Link to="/connexion"><h6>Connexion</h6></Link>
              </a>
            </li>
          </ul>
        </div>
      </nav>
    </Fragment>

  );
}
