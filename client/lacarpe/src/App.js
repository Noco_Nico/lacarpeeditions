import React from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Header from "./components/Header";
import Atelier from "./pages/Atelier";
import Actualites from "./pages/Actualites";
import Artistes from "./pages/Artistes";
import Boutique from "./pages/Boutique";
import Connexion from "./pages/Connexion.js";
import AjoutNouvelUtilisateur from './pages/AjoutNouvelUtilisateur';
import Amose from './pages/Amose';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import './App.css';

function App(props) {

  return (

    <Router>
      <div id="header">
        <Header />
        <Switch>
          <Route path="/atelier">
            <Atelier />
          </Route>
          <Route path="/artistes">
            <Artistes />
          </Route>
          <Route path="/amose">
            <Amose />
          </Route>
          <Route path="/connexion">
            <Connexion />
          </Route>
          <Route path="/ajoutNouvelUtilisateur">
            <AjoutNouvelUtilisateur />
          </Route>
          <Route path="/boutique">
            <Boutique />
          </Route>
          <Route path="/">
            <Actualites />
          </Route>
        </Switch>
      </div>
    </Router>

  );
}

export default App;
