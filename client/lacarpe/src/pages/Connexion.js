import React, { Fragment } from "react";
import { Link } from 'react-router-dom';




export default function Connexion() {
    return (
        <Fragment>
            <div className="row" >
                <div className="container col-md-3" >
                    <form >
                        <h3 id="txtContenu" className="text-center">Connexion</h3>
                        <p id="space" />
                        <p id="space" />
                        <div className="form-group ml-auto" id="txtContenu" >
                            <label >Adresse Mail</label>
                            <input type="email" className="form-control" placeholder="Entrez votre mail" />
                        </div>

                        <div className="form-group">
                            <label>Mot de passe</label>
                            <input type="password" className="form-control" placeholder="Entrez votre mot de passe" id="txtContenu" />
                        </div>
                        <p id="space" />
                        <p id="space" />
                        <button type="submit" className="btn btn-outline-dark btn-block ">ENVOYER</button>

                        <Link to="/ajoutNouvelUtilisateur">Nouvel Utilisateur ?</Link>
                    </form>
                </div>
            </div>
        </Fragment>
    );
}