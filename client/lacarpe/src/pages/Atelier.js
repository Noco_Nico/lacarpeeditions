import React, { Fragment } from "react";
import Carousel from "../components/Carousel.js";
import Description from "../components/Description.js";



export default function Atelier() {

    return (
        <Fragment>
            <p className="pt-5" />
            <h1 className="text-center">L'Atelier</h1>
            <p className="pt-5" />
            <div className="ml-5 mr-5">
                <Carousel />
                <p className="pt-5" />
                <p className="pt-5" />
                <Description />
            </div>
        </Fragment>

    );
}