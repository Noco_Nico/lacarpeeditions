import React, { Fragment } from "react";




function AjoutNouvelUtilisateur() {
  return (
    <Fragment>
      <div className="row">
        <div className="container">

          <form >
            <p className="pt-5" />
            <p className="pt-5" />
            <h3 className="text-center" id="txtContenu">CRÉATION DE COMPTE</h3>
            <p className="pt-5" />
            <div className="form-group lead " id="txtContenu" >
              <div className="container" >
                <div className=" ml-5 mr-5">

                  <div class="form-group row">
                    <label for="nom" class="col-sm-3 col-form-label">Nom</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" placeholder="Nom" />
                    </div>
                  </div>
                  <p className="pt-1" />

                  <div class="form-group row">
                    <label for="prenom" class="col-sm-3 col-form-label">Prenom</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" placeholder="Prenom" />
                    </div>
                  </div>
                  <p className="pt-1" />
                  <div class="form-group row">
                    <label for="username" class="col-sm-3 col-form-label">Nom Utilisateur</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" placeholder="Nom Utilisateur" />
                    </div>
                  </div>
                  <p className="pt-1" />
                  <div class="form-group row">
                    <label for="inputPassword" class="col-sm-3 col-form-label">Mot de Passe</label>
                    <div class="col-sm-9">
                      <input type="password" class="form-control" placeholder="Mot de Passe" />
                    </div>
                  </div>
                  <p className="pt-1" />
                  <div class="form-group row">
                    <label for="inputPassword" class="col-sm-3 col-form-label">Vérification</label>
                    <div class="col-sm-9">
                      <input type="password" class="form-control" placeholder=" Vérification Mot de Passe" />
                    </div>
                  </div>
                  <p className="pt-1" />
                  <div class="form-group row">
                    <label for="inputMail" class="col-sm-3 col-form-label">Adresse Mail</label>
                    <div class="col-sm-9">
                      <input type="mail" class="form-control" placeholder="Adresse Mail" />
                    </div>
                  </div>
                  <h3 className="pt-1 text-center" >ADRESSE</h3>
                  <p className="pt-1" />
                  <div class="form-group row">
                    <label for="numRue" class="col-sm-3 col-form-label">Numero</label>
                    <div class="col-sm-9">
                      <input type="number" class="form-control" placeholder="numRue" />
                    </div>
                  </div>
                  <p className="pt-1" />
                  <div class="form-group row">
                    <label for="rue" class="col-sm-3 col-form-label">Rue</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" placeholder="Rue" />
                    </div>
                  </div>
                  <p className="pt-1" />
                  <div class="form-group row">
                    <label for="cp" class="col-sm-3 col-form-label">Code Postal</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" placeholder="cp" />
                    </div>
                  </div>
                  <p className="pt-1" />
                  <div class="form-group row">
                    <label for="ville" class="col-sm-3 col-form-label">Ville</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" placeholder="Ville" />
                    </div>
                  </div>
                  <p className="pt-1" />
                  <div class="form-group row">
                    <label for="numTel" class="col-sm-3 col-form-label">Numéro de Téléphone</label>
                    <div class="col-sm-9">
                      <input type="mail" class="form-control" placeholder="Numéro de téléphone" />
                    </div>
                  </div>
                </div>
              </div>
              <p className="pt-1" />
              <div className="text-center">
                <button type="submit" className="btn btn-primary">ENVOYER</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </Fragment>
  );
}

export default AjoutNouvelUtilisateur;