import React, { Fragment } from "react";
import shopAmo from "../images/shopAmo.jpg";
import shopErone from "../images/shopErone.jpg";
import shopSpyre from "../images/shopSpyre.jpg"
import { Link } from 'react-router-dom';

export default function Boutique() {
    return (
        <Fragment>
            <p id="space" />
            <div className="container text-center">
                <p id="space" />
                <div className="row">
                    <div className="col-4">
                        <header className="shopAmoHeader text-center">
                            <Link to="/amose">
                                <img src={shopAmo}
                                    height={600}
                                    width={400} alt="shopAmo" />
                                <h3>AMOSE </h3>
                            </Link>
                        </header>
                    </div>
                    <div className="col-4">
                        <header className="shopEroneHeader text-center">
                            <a href="https://salsifis.tumblr.com/" target="blank">
                                < img src={shopErone}
                                    height={600}
                                    width={400} alt="shopErone" />
                                <h3>ERONE </h3>
                            </a>
                        </header>
                    </div>
                    <div className="col-4">
                        <header className="shopSpyreHeader text-center">
                            <a href="https://www.flickr.com/photos/spyreone/" target="blank">
                                < img src={shopSpyre}
                                    height={600}
                                    width={400} alt="shopSpyre" />
                                <h3>SPYRE </h3>
                            </a>
                        </header>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}

